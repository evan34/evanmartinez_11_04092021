import React from 'react'

export const createRatingsElements = (dataRatings, totalRatings = 5, star, greyStar) => {
  const ratingsElements = []
  for (let i = 1; i <= totalRatings; i++) {
    if (dataRatings >= i) {
      ratingsElements.push(<img src={star} alt="etoile" className="rating__img" key={i} />)
    } else {
      ratingsElements.push(<img src={greyStar} alt="etoile" className="rating__img" key={i} />)
    }
  }
  return ratingsElements
}
