import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Footer from './components/Footer'
import Header from './components/Header'
import About from './pages/About'
import Home from './pages/Home'
import Logding from './pages/Logding'
import NotFound from './pages/NotFound'

export default class App extends Component {
  render () {
    return (
      <Router>
        <Header />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/a-propos" component={About} />
          <Route path="/logements/:id" component={Logding} />
          <Route component={NotFound} />
        </Switch>
        <Footer />
      </Router>
    )
  }
}
