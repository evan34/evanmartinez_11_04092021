import React, { Component } from 'react'
import { Link } from 'react-router-dom'

/**
 * 404 Error page
 */
export default class NotFound extends Component {
  render () {
    return (
      <div className="container">
        <p className="container__error">404</p>
        <p className="container__oups">Oups! La page que vous demandez n'existe pas.</p>
        <Link to="/" className="nav__links--item container__homeLink">Retourner sur la page d’accueil</Link>
      </div>
    )
  }
}
