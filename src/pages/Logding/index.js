import React, { Component } from 'react'
import { Redirect } from 'react-router'
import logdings from '../../data/logdings.json'
import LogdingContainer from '../../components/LogdingContainer'

export default class Logding extends Component {
  state = {
    isLoaded: false
  }

  componentDidMount = () => {
    this.setState({
      isLoaded: true,
      data: this.getData()
    })
  }

  /**
   * Fetch json logdings file data
   * @returns data
   */
  getData = () => {
    const param = this.props.match.params.id
    let logding = {}
    logdings.map(item => {
      if (item.id === param) {
        logding = item
      }
      return item
    })
    return logding
  }

  render () {
    const urlParams = this.props.match
    const { isLoaded } = this.state
    if (!isLoaded) {
      return <div>Chargement...</div>
    } else {
      const { data } = this.state
      const param = this.props.match.params.id
      if(data.id !== param) return <Redirect to="/NotFound" />
      return (
        <>
          <section className="logdingSection">
            <LogdingContainer
              key={data.id}
              title={data.title}
              rating={data.rating}
              lodging={data}
              params={urlParams}
            />
          </section>

        </>
      )
    }
  }
}
