import React, { Component } from 'react'
import values from '../../data/values.json'
import Collapse from '../../components/Collapse'
import Banner from '../../components/Banner'

export default class About extends Component {
  constructor (props) {
    super(props)
    this.state = {
      items: [],
      isLoaded: false
    }
  }

  componentDidMount () {
    this.setState({
      items: values,
      isLoaded: true
    })
  }

  render () {
    const params = this.props.match.path
    const { items, isLoaded } = this.state
    if (!isLoaded) {
      return <div>Chargement...</div>
    } else {
      return (
        <main className="mainAbout">
          <div className="main__banner--about">
            <Banner params={params} />
          </div>
          <div className="container__dropdowns">
            {items.map((item, index) => {
              return <Collapse key={index} name={item.name} text={item.text} params={params} />
            })}
          </div>
        </main>
      )
    }
  }
}
