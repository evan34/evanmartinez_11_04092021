import React, { Component } from 'react'
import logdings from '../../data/logdings.json'
import Card from '../../components/Card'
import Banner from '../../components/Banner'

export default class Home extends Component {
  state = {
    items: [],
    isLoaded: false
  }

  componentDidMount = () => {
    this.setState({
      items: logdings,
      isLoaded: true
    })
  }

  render() {
    const params = this.props.match.path
    const { items, isLoaded } = this.state
    if (!isLoaded) {
      return <div>Chargement...</div>
    } else {
      return (
        <main className="main">
          <div className="main__banner">
            <Banner params={params} />
            <h1 className="text"><span>Chez vous, </span><span>partout et ailleurs</span></h1>
          </div>
          <section className="main__section">
            {items.map(item => {
              return (
                <Card
                  key={item.id}
                  id={item.id}
                  title={item.title}
                  cover={item.cover}
                />
              )
            })}
          </section>
        </main>
      )
    }
  }
}
