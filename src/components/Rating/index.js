import React, { Component } from 'react'
import star from '../../assets/svg/star.svg'
import greyStar from '../../assets/svg/greyStar.svg'
import { createRatingsElements } from '../../utils'

export default class Rating extends Component {
  render () {
    const dataRatings = parseInt(this.props.rating, 10)
    const totalRatings = 5

    return (
      <div className="rating">
        {
          createRatingsElements(dataRatings, totalRatings, star, greyStar).map(el => el)
        }
      </div>
    )
  }
}
