import React, { Component } from 'react'
import downArrow from '../../assets/svg/downArrow.svg'
import upArrow from '../../assets/svg/upArrow.svg'

export default class Collapse extends Component {
  state = {
    open: false
  }

  onToggle () {
    this.setState({ open: !this.state.open })
  }

  render () {
    const { params, name, text } = this.props
    return (
      // dropdownContainerDetails / dropdownDetails: logding page dropdowns
      // dropdownContainer / dropdown: about page dropdowns
      <div className={params.path === '/logements/:id' ? 'dropdownContainerDetails' : 'dropdownContainer'}>
        <div className={params.path === '/logements/:id' ? 'dropdownDetails' : 'dropdown'}
          onClick={() => this.onToggle()}
        >
          <h2>{name}</h2>
          {this.state.open
            ? <div className="upArrow arrow">
              <img src={upArrow} alt="down arrow" />
            </div>
            : <div className="downArrow arrow">
              <img src={downArrow} alt="up arrow" />
            </div>
          }
        </div>
        <div className={this.state.open ? 'panel-collapse' : 'panel-collapse panel-close'}>
          {
            Array.isArray(text)
              ? text.map((item, index) => <p key={index}>{item}</p>)
              : <p>{text}</p>
          }
        </div>
      </div>
    )
  }
}
