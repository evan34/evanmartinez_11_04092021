import React, { Component } from 'react'
import logo from '../../assets/svg/logo-footer.svg'

export default class Footer extends Component {
  render () {
    const date = new Date().getFullYear()
    return (
      <footer className="footer">
        <img src={logo} alt="Logo de kasa" className="footer__img" />
        <p className="footer__text">&#169; {date} Kasa, all rights reserved</p>
      </footer>
    )
  }
}
