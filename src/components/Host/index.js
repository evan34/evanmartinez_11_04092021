import React, { Component } from 'react'

export default class Host extends Component {
  render () {
    const { name, picture } = this.props
    return (
      <div className="host">
        <h3 className="host__name">{name}</h3>
        <img src={picture} alt="portrait de l'hote" className="host__img" />
      </div>
    )
  }
}
