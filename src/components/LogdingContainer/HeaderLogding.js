import React, { Component } from 'react'
import Slider from '../Slider'
import Tag from '../Tag'
import Rating from '../Rating'
import Host from '../Host'

export default class HeaderLogding extends Component {
  render () {
    const { data } = this.props
    return (
      <div className="headerLogding">
        <Slider
          key={data.id}
          pictures={data.pictures}
        />
        <div className="descriptionLogding">
          <div className="logdingTitles">
            <h1>{data.title}</h1>
            <p>{data.location}</p>
          </div>
          <Host
            key={data.id}
            name={data.host.name}
            picture={data.host.picture}
          />
        </div>
        <div className="infos">
          <div className="tags">
            {data.tags.map((tag, index) => (
              <Tag
                key={index}
                value={tag}
              />
            ))}
          </div>
          <Rating
            rating={data.rating}
          />
        </div>
      </div>
    )
  }
}
