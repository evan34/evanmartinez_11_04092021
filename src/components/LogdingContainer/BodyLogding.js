import React, { Component } from 'react'
import Collapse from '../Collapse'

export default class BodyLogding extends Component {
  render () {
    const params = this.props.params
    const { data } = this.props
    return (
      <div className="bodyLogding">
        <Collapse
          key={data.description}
          name="Description"
          text={data.description}
          params={params}
        />
        <Collapse
          key={data.id}
          name="&Eacute;quipements"
          text={data.equipments}
          params={params}
        />
      </div>
    )
  }
}
