import React, { Component } from 'react'
import HeaderLogding from './HeaderLogding'
import BobyLogding from './BodyLogding'

export default class LogdingContainer extends Component {
  render () {
    const params = this.props.params
    const { lodging } = this.props
    return (
      <div className="logdingContainer">
        <HeaderLogding data={lodging} />
        <BobyLogding data={lodging} params={params} />
      </div>
    )
  }
}
