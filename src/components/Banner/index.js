import React, { Component } from 'react'
import homeBanner from '../../assets/img/home.jpg'
import aboutBanner from '../../assets/img/about.jpg'

export default class Banner extends Component {
  render() {
    const { params } = this.props
    console.log(params)
    return (
      <>
        {
          params === '/' ?
            <>
              <img src={homeBanner} alt="Paysage" />
              <h1 className="text"><span>Chez vous, </span><span>partout et ailleurs</span></h1>
            </>
            :
          params === '/a-propos' ?
            <>
              <img src={aboutBanner} alt="Paysage" />
            </>
              :
            <></>  
        }
      </>
    )
  }
}

