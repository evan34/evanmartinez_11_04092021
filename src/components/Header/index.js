import React, { Component } from 'react'
import { Link, NavLink } from 'react-router-dom'
import logo from '../../assets/svg/logo.svg'

export default class Header extends Component {
  render () {
    return (
      <header className="header">
        <nav className="nav">
          <Link to="/" className="nav__logo">
            <img src={logo} alt="Logo de kasa" />
          </Link>
          <ul className="nav__links">
            <li>
              <NavLink exact to="/" activeClassName="selected" className="link">Accueil</NavLink>
            </li>
            <li>
              <NavLink to="/a-propos" activeClassName="selected" className="link">A propos</NavLink>
            </li>
          </ul>
        </nav>
      </header>
    )
  }
}
