import React, { Component } from 'react'
import nextArrow from '../../assets/svg/nextArrow.svg'
import prevArrow from '../../assets/svg/prevArrow.svg'

export default class BtnSlider extends Component {
  render () {
    return (
      <button
        onClick={this.props.moveSlide}
        className={this.props.direction === 'next' ? 'btn-slide next' : 'btn-slide prev'}
      >
        <img src={this.props.direction === 'next' ? nextArrow : prevArrow} alt="bouton" />
      </button>
    )
  }
}
