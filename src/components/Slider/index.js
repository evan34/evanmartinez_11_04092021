import React, { Component } from 'react'
import BtnSlider from './BtnSlider'

export default class Slider extends Component {
  state = {
    index: 1
  }

  nextSlide = () => {
    if (this.state.index !== this.props.pictures.length) {
      this.setState({ index: this.state.index + 1 })
    } else if (this.state.index === this.props.pictures.length) {
      this.setState({ index: 1 })
    }
  }

  prevSlide = () => {
    if (this.state.index !== 1) {
      this.setState({ index: this.state.index - 1 })
    } else if (this.state.index === 1) {
      this.setState({ index: this.props.pictures.length })
    }
  }

  render () {
    return (
      <div className="sliderContainer">
        {this.props.pictures.map((item, index) => {
          return (
            <div
              key={index}
              className={this.state.index === index + 1 ? 'slide active' : 'slide'}
            >
              <img src={item} alt="détails du logement" />
            </div>
          )
        })}
        {
          this.props.pictures.length > 1 ?
            <>
              <BtnSlider moveSlide={this.nextSlide} direction={'next'} />
              <BtnSlider moveSlide={this.prevSlide} direction={'prev'} />
            </>
            :
            <></>
        }
      </div>
    )
  }
}
