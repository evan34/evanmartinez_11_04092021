import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class Card extends Component {
  render () {
    const { id, cover, title } = this.props
    return (
      <Link to={`/logements/${id}`}>
        <div className="card">
          <img className="card__cover" src={cover} alt="" />
          <h3 className="card__title">{title}</h3>
        </div>
      </Link>
    )
  }
}
